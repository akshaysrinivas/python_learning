def append_file(file_path1, file_path2):
    with open(file_path1) as file1:
        file1_contents = file1.read()
    with open(file_path2, "a") as file2:
        file2.write(file1_contents)
    return True

append_file("files/animals.txt", "files/goat.txt")