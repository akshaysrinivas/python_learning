def foo(*args):
    str_list = [i.upper() for i in args]
    str_list.sort()
    return str_list

print(foo('akshay', 'srinivas', 'cat', 'dog'))