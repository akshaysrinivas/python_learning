""" Assignment """
def string_converter(input_string):
    string_capitalize = input_string.capitalize()
    question_words = ("how", "what", "when", "which")
    if input_string.startswith(question_words):
        return "%s?"%string_capitalize
    else:
        return string_capitalize

message_box = []
while True:
    msg = input("Say something; ")
    if msg == '/end':
        break
    else:
        message_box.append(string_converter(msg))

print(" ".join(message_box))

#print(string_converter("what world!"))
"""
i = 0
msg = ''
user_msg = []

while True:
    msg = input("Say something: ")
    if msg == '/end':
        break
    else:
        user_msg.append(msg)
    i = i + 1

#print(f"\n {user_msg} \n")

for i in user_msg:
    print(f"{i}")
"""