atm_temperatures = [23.4, 34.45, 19.0, 20.46, 1, 0]
week_days = {"Monday" : 2, "Sunday" : 1, "Wednesday" : 4, "Friday" : 6}

def mean(value):
    if type(value) == list:
        ab_mean = sum(value)/len(value)
    else:
        ab_mean = sum(value.values())/len(value)
    return ab_mean

print(mean(week_days))