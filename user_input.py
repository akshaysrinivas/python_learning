""" User input example """

def temperature(temp):
    if temp < 15:
        return 'Cold'
    else:
        return 'Warm'

user_input = input('Temperature value : ')
print("\n ", user_input, " ", type(user_input), "\n")

print('\n It\'s really ', temperature(float(user_input)), ' today.\n')