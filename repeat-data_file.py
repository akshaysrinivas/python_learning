def append_loop_data(file_path):
    with open(file_path, "a+") as file:
        file.seek(0)
        content_file = file.read()
        for i in range(2):
            file.write(content_file)
    return True

append_loop_data("files/data.txt")