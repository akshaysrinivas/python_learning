def mix_sep(list_in):
    return [i for i in list_in if isinstance(i, str)]

print(mix_sep(["Akshay", 2.0, 3, "Love", 8.7, "You"]))

def mix_list(in_list):
    return [0 if isinstance(i, str) else i for i in in_list]

print(" ", mix_list(["Akshay", 2.0, 3, "Love", 8.7, "You"]))
"""
def sum_str(my_list):
    total_sum = 0
    for i in my_list:
        total_sum = total_sum + float(i)
    return [total_sum]

"""
def sum_str(my_list):
    return sum([ float(i) for i in my_list ])
    
print(sum_str(['2.3', '1.4', '5.6', '7.3', '2.2']))

def conca(a, b):
    return "%s %s" % (a, b)

print(conca("Akshay", "Srinivas"))