""" String formatting in latest f-string method """
import datetime

def string_for(name, date_today):
    return f"\n Hi {name} what's your plan for {date_today} \n"

date_today = datetime.datetime.date(datetime.datetime.now())
name = input("\n Enter your name : ")

print(string_for(name,date_today))